<!--/* Author : Munira *-->
<?php include_once './lib/settings.php'; ?>
<?php include_once './lib/connection.php'; ?>
<?php include_once './segments/header_segments.php'; ?>
<body>
    <!--preloader-->
    <div class="preloader">
        <div class="spinner"></div>
    </div>
    <!--//preloader-->

    <!--header-->
    <header class="head" role="banner">
        <!--wrap-->
        <div class="wrap clearfix">
            <a href="index.html" title="SocialChef" class="logo"><img src="images/ico/logo.png" alt="SocialChef logo" /></a>


            <!--top navbar manus item start here-->
            <?php include_once './segments/top_navbar_menu_item.php'; ?>
            <!--top navbar manus item end here-->
        </div>
    </header>
    <!--//header-->

    <!--main-->
    <main class="main" role="main">
        <!--wrap-->
        <div class="wrap clearfix">
            <!--breadcrumbs-->
            <nav class="breadcrumbs">
                <ul>
                    <li><a href="index.php" title="Home">Home</a></li>
                    <li>Recipes</li>
                </ul>
            </nav>
            <!--//breadcrumbs-->

            <!--row-->
            <div class="row">
                <header class="s-title">
                    <h1>Recipes</h1>
                </header>

                <!--content-->
                <section class="content three-fourth">
                    <!--entries-->
                    <div class="entries row">


                        <!--item-->

                        <?php
                        $sql = "SELECT r.*,
                                IFNULL((SUM(rr.rating)/count(rr.id)),0) as rating,
                                IFNULL((SELECT count(w.id) FROM wishlist as w WHERE w.recipes_id=r.id),0) as wish 
                                FROM `recipes` as r
                                LEFT JOIN recipes_rating as rr ON r.id=rr.recipes_id 
                                WHERE r.category_id='" . $_GET['cid'] . "' 
                                GROUP BY r.id 
                                LIMIT 6";
                        $datasql = $db->query($sql);
                        $chk = $datasql->rowCount();
                        if ($chk != 0) {
                            foreach ($datasql as $row) :
                                ?>
                                <div class="entry one-third">
                                    <figure>

                                        <img src="<?= SITE_IMG_PATH ?><?php echo $row['image']; ?>" style="height: 190px !important; width:280px;" alt="recipes_image"  />

                                        <figcaption><a href="recipe_view.php?rec_id=<?= $row['id']; ?>"><i class="icon icon-themeenergy_eye2"></i> <span>View recipe</span></a></figcaption>
                                    </figure>

                                    <div class="container">
                                        <h2 style="height: 60px;"><a href="recipe_view.php?rec_id=<?= $row['id']; ?>"><?= $row['name']; ?></a></h2> 
                                        <div class="actions">

                                            <div>
                                                <div class="comments" title="please Leave a Rating">
                                                    <span class="rating">
                                                        <input type="radio" id="star5" name="rating" value="5" />
                                                        <label data="<?= $row['id']; ?>" class = "full" for="star5" title="Awesome - 5 stars"></label>
                                                        <input type="radio" id="star4" name="rating" value="4" />
                                                        <label data="<?= $row['id']; ?>" class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                                        <input type="radio" id="star3" name="rating" value="3" />
                                                        <label data="<?= $row['id']; ?>" class = "full" for="star3" title="Meh - 3 stars"></label>
                                                        <input type="radio" id="star2" name="rating" value="2" />
                                                        <label data="<?= $row['id']; ?>" class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                                        <input type="radio" id="star1" name="rating" value="1" />
                                                        <label data="<?= $row['id']; ?>" class = "full" for="star1" title="Sucks big time - 1 star"></label>

                                                    </span>
                                                    <span class="rating_result" id="rating_result_<?= $row['id']; ?>"><?= number_format($row['rating']) ?></span>


                                                </div>
                                                <div class="likes wishbar" data-id="<?= $row['id']; ?>" customer-id="<?= $customer_id ?>" title="Add to Wishlist">
                                                    <i  class="fa fa-heart wishresult"></i> <span id="wish_result_<?= $row['id']; ?>"><?= number_format($row['wish']) ?></span>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                            <?php endforeach;
                            ?>
                            <div class="quicklinks">
                                <a href="#" class="button">More recipes</a>
                                <a href="javascript:void(0)" class="button scroll-to-top">Back to top</a>
                            </div>
                            <?php
                        }
                        else {
                            ?>
                            <h3>No Recipes Found</h3>
                            <?php
                        }
                        ?>

                        <!--item-->


                    </div>
                    <!--//entries-->
                </section>
                <!--//content-->

                <!--right sidebar-->
                <aside class="sidebar one-fourth">
                    <div class="widget">
                        <ul class="categories right">
                            <li class="active"><a href="#">All recipes</a></li>




                            <?php
                            $sql = "SELECT * FROM recipes_category";

                            foreach ($db->query($sql) as $category) :
                                ?>

                                <li<?php
                                if ($_GET['cid'] == $category['id']) {
                                    ?>
                                        class="active" 
                                        <?php
                                    }
                                    ?>>
                                    <a  href="category.php?cid=<?php echo $category['id']; ?>" title="Icons"> <?php echo $category['name']; ?></a>
                                </li>


                                <?php
                            endforeach;
                            ?>

                        </ul>
                    </div>
                    <!--                   add sector-->
                    <div class="widget">
                        <h3>Advertisment</h3>
                        <a href="#"><img src="images/advertisment.jpg" alt="" /></a>
                    </div>
                </aside>
                <!--//right sidebar-->
            </div>
            <!--//row-->
        </div>
        <!--//wrap-->
    </main>
    <!--//main-->

    <!--footer-->
    <?php include_once './segments/footer_part.php'; ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="ajax/rating.js"></script>

    <script src="ajax/wishlist.js"></script>


    <!--//footer end-->
    <style type="text/css">
        .wishbar{
            line-height: 30px; font-size: 14px;
        }
        .wishresult{
            margin-top:9px;
        }
        .rating_result{
            float: right; margin-left:12px; margin-top: 5px; font-size: 14px;
        }

        .rating { 
            border: none;
            float: left;
        }

        .rating > input { display: none; } 
        .rating > label:before { 
            margin: 3px;
            font-size: 1.50em;
            font-family: FontAwesome;
            display: inline-block;
            content: "\f005";
        }

        .rating > .half:before { 
            content: "\f089";
            position: absolute;
        }

        .rating > label { 
            color: #ddd; 
            float: right; 
        }

        /***** CSS Magic to Highlight Stars on Hover *****/

        .rating > input:checked ~ label, /* show gold star when clicked */
        .rating:not(:checked) > label:hover, /* hover current star */
        .rating:not(:checked) > label:hover ~ label { color: #FFD700;  } /* hover previous stars in list */

        .rating > input:checked + label:hover, /* hover current star when changing rating */
        .rating > input:checked ~ label:hover,
        .rating > label:hover ~ input:checked ~ label, /* lighten current selection */
        .rating > input:checked ~ label:hover ~ label { color: #FFED85;  } 
    </style>



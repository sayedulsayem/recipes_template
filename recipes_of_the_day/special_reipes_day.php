<div class="featured two-third">
    <header class="s-title">
        <h2 class="ribbon">Special Recipe of the Day</h2>
    </header>
    <?php
    $sql = "SELECT recipes.*,COUNT(recipes_view.id) FROM `recipes` 
LEFT JOIN recipes_view ON recipes.id=recipes_view.recipes_id
GROUP BY recipes.id order by COUNT(recipes_view.id) DESC LIMIT 1";
    foreach ($db->query($sql)as $row) {
        $recipe = $row;
        $chefId=$recipe['id'];
    }
    ?>
    

    <article class="entry">
        <figure>
            <img src="<?= SITE_IMG_PATH ?><?php echo $recipe['image']; ?>" style="height: 560px!important; width:780px;" alt="special_recipes_day_image" />
            <figcaption><a href="<?= APP_PATH ?>recipe.php"><i class="icon icon-themeenergy_eye2"></i> <span>View recipe</span></a></figcaption>
        </figure>
        <div class="container">
            <h2><a href="<?= APP_PATH ?>recipe.php"><?php echo $recipe['name'];?></a></h2>
            <p><?php echo $recipe['directions'];?> </p>
            <div class="actions">
                <div>
                    <a href="<?= APP_PATH ?>recipe_view.php" class="button">See the full recipe</a>
                    <div class="more"><a href="recipes2.html">See past recipes of the day</a></div>
                </div>
            </div>
        </div>
    </article>
</div>
<!--    //SELECT * FROM `recipes` WHERE `id`=(SELECT `recipes_id` FROM recipes_view  GROUP BY recipes_id ORDER BY COUNT(id) DESC LIMIT 1)
//
/*  
 * 
SELECT recipes.*,COUNT(recipes_view.id) FROM `recipes` 
LEFT JOIN recipes_view ON recipes.id=recipes_view.recipes_id
GROUP BY recipes.id order by COUNT(recipes_view.id) DESC LIMIT 1
 * 
 */
//-->
<div class="featured one-third">
    <header class="s-title">
        <h2 class="ribbon star">Featured member</h2>
    </header>
    <?php
    $sql = "SELECT * FROM `chef_recipes`, chef_detail WHERE chef_recipes.recipes_id=$chefId AND chef_recipes.chef_id=chef_detail.id ";

    foreach ($db->query($sql)as $row) {
        $chef = $row;
    }
    if (isset($chef)){
    ?>
    <article class="entry">
        <figure>
            <img src="<?= SITE_IMG_PATH ?><?php echo $chef['image']; ?>" style="height: 520px !important; width:372px;" alt="chef_image" />
            <figcaption><a href="#"><i class="icon icon-themeenergy_eye2"></i> <span>View member</span></a></figcaption>
        </figure>
        <div class="container">
            <h2><a href="#"><?php echo $chef['name']; ?></a></h2>
            <blockquote><i class="fa fa-quote-left"></i><?php echo $chef['descriptions']; ?></blockquote>

            <div class="actions">
                <div>
                    <a href="#" class="button" alt="under_constructions">Check out her recipes</a>
                    <!--                    <div class="more"><a href="#">See past featured members</a></div>-->
                </div>
            </div>

        </div>
        <?php }else
        {?>
            <div class='alert alert-danger'><strong>Not found !</strong> This recipes chef is not found .
            this recipes has no chef or this recipes comes from customer. </div>
        <?php } ?>

    </article>
</div>
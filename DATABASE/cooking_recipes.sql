-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 13, 2017 at 12:30 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cooking_recipes`
--

-- --------------------------------------------------------

--
-- Table structure for table `chef_detail`
--

CREATE TABLE `chef_detail` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `descriptions` text NOT NULL,
  `date` date NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chef_detail`
--

INSERT INTO `chef_detail` (`id`, `name`, `image`, `descriptions`, `date`, `is_active`) VALUES
(14, 'Munira', '1497252555lucky.jpg', 'ddssssssssssssssssssss\r\nsssssssssssssssssssssss\r\nsssssssssssssssssssss', '2017-06-12', 0),
(16, 'monowar', '1497267720lucky.jpg', 'cooking', '2017-06-12', 1),
(17, 'sharmin lucky', '1497268082sharmin-lucky (1).jpg', 'asdfg', '2017-06-12', 0),
(18, 'siddika kobir', '1497268512siddika_kobir.png', 'assdfgg', '2017-06-12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `chef_recipes`
--

CREATE TABLE `chef_recipes` (
  `id` int(11) NOT NULL,
  `chef_id` int(20) NOT NULL,
  `recipes_id` int(20) NOT NULL,
  `date` date NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chef_recipes`
--

INSERT INTO `chef_recipes` (`id`, `chef_id`, `recipes_id`, `date`, `is_active`) VALUES
(1, 14, 15, '2017-05-20', 0),
(2, 17, 20, '2017-05-20', 0),
(3, 18, 26, '1970-01-01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `contact_request`
--

CREATE TABLE `contact_request` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `descriptions` text NOT NULL,
  `date` date NOT NULL,
  `status` enum('Pending','Reject','In Progress','Complete') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_request`
--

INSERT INTO `contact_request` (`id`, `name`, `email`, `subject`, `descriptions`, `date`, `status`) VALUES
(1, 'sdadas', 'monira@gmail.com', '', 'sdsasadas dasd asd', '2017-05-29', 'Pending'),
(2, '', '', '', '', '2017-05-29', 'Pending'),
(3, '', '', '', '', '2017-05-29', 'Pending'),
(4, '', '', '', '', '2017-05-29', 'Pending'),
(5, '', '', '', '', '2017-05-29', 'Pending'),
(6, '', '', '', '', '2017-05-29', 'Pending'),
(7, '', '', '', '', '2017-05-29', 'Pending'),
(8, '', '', '', '', '2017-05-29', 'Pending'),
(9, '', '', '', '', '0000-00-00', 'Reject'),
(10, '', '', '', '', '0000-00-00', ''),
(11, 'ssas', 'munirAEIN@GAMIL.COM', '', 'sds', '2017-05-31', 'Pending'),
(12, '', '', '', '', '0000-00-00', 'Pending'),
(13, '', '', '', '', '0000-00-00', 'Complete'),
(14, '', '', '', '', '0000-00-00', 'In Progress');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_type` enum('user','admin') NOT NULL,
  `date` date NOT NULL,
  `status` enum('active','inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `name`, `email`, `password`, `user_type`, `date`, `status`) VALUES
(1, 'Anu', 'anu@gmail.com', '123', 'user', '2017-05-24', 'active'),
(6, 'jombi', 'jombi@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'user', '2017-06-12', ''),
(7, 'munira', 'monira@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'user', '0000-00-00', 'active'),
(9, 'munira', 'muniraein@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'user', '2017-06-12', ''),
(11, 'Munira', 'monira@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'admin', '2017-05-28', 'active'),
(13, 'rashed lelin', 'rashed@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e', 'admin', '2017-06-12', 'active'),
(14, 'sayem', 'sayem@gmail.com', '709ca0cd9eb443ba7bb7e3bb58e69706', 'admin', '0000-00-00', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `customer_recipes_request`
--

CREATE TABLE `customer_recipes_request` (
  `id` int(11) NOT NULL,
  `customer_id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `ingredients` varchar(255) NOT NULL,
  `directions` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `status` enum('Approve','Cancel','Pending') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_recipes_request`
--

INSERT INTO `customer_recipes_request` (`id`, `customer_id`, `name`, `image`, `ingredients`, `directions`, `date`, `status`) VALUES
(1, 1, 'bangul vorta', '', 'zzzzzzzzzzzz', 'zzzzzzzzzzzz', '2017-05-22', ''),
(2, 2, 'Test', '', '', '', '2017-05-26', ''),
(3, 2, 'Test', '', '', '', '2017-05-26', ''),
(4, 2, 'Test', '', '', '', '2017-05-26', ''),
(5, 2, 'Test', '', '', '', '2017-05-26', ''),
(6, 2, 'Test', '', '', '', '2017-05-26', ''),
(7, 2, 'Test', '', 'sdasd asdsa dsa dsa dsad as dsadas', 'dsadsadas dsadasd asdasdasdas das', '2017-05-26', ''),
(8, 2, 'Test', '', 'sdasd asdsa dsa dsa dsad as dsadas', 'dsadsadas dsadasd asdasdasdas das', '2017-05-26', ''),
(9, 2, 'Test', '', 'sdasd asdsa dsa dsa dsad as dsadas', 'dsadsadas dsadasd asdasdasdas das', '2017-05-26', ''),
(10, 2, 'Test', '', 'sdasd asdsa dsa dsa dsad as dsadas', 'dsadsadas dsadasd asdasdasdas das', '2017-05-26', ''),
(11, 2, 'sdfdsfsdfdsf', '', 'sdfdsfsdfsdf', 'sdfdsfdsfsdfsd', '2017-05-26', ''),
(12, 2, 'zxcxzcxz', '', 'xzcxzcxz', 'cxzcxzc', '2017-05-26', ''),
(13, 2, 'dsfdsfdsf', '', 'dsfdsfsdf', 'sdfdsf', '2017-05-26', ''),
(14, 2, 'cxvcxvcxvcx', '', 'xcvcxvxv', 'xcvcxvcxvc', '2017-05-26', ''),
(15, 2, 'sdfsdfafasfsfa', '', 'sdafdsaasdf sdf ', 'sdaf sdaf sdaf sdafs ', '2017-05-26', ''),
(16, 2, 'asdsadasdsad', '', 'asdsadsadsa', 'asdasdsadasdsa', '2017-05-26', ''),
(17, 2, 'sdfdsfsdfdsfsdfsdf', '', 'sdfdsfsdfdsf', 'sdfsdfsdfdsfsd', '2017-05-26', ''),
(18, 2, 'sdfdsfsdfdsfsdfsdf', '', 'sdfdsfsdfdsf', 'sdfsdfsdfdsfsd', '2017-05-26', ''),
(19, 2, 'sdsdsdsds', '', 'sdsddsds', 'sddsddsdds', '2017-05-26', ''),
(20, 2, 'asdsadsadsad', '', 'asdsadsadsad', 'asdasdsadsa', '2017-05-26', ''),
(21, 2, 'asdsadsadsad', '2015-07-09-01-08-01-508 - Copy.jpg', 'asdsadsadsad', 'asdasdsadsa', '2017-05-26', ''),
(22, 2, 'asdsadsad', '', 'sadsads', 'asdsadsadsad', '2017-05-26', ''),
(23, 2, 'sdfdsfdsfdsfdsf', '2015-07-09-01-08-01-508 - Copy.jpg', 'sdfsdfdsfds', 'dsfdsfdsfdsfds', '2017-05-26', ''),
(24, 2, 'Test', '2015-07-09-01-08-01-508 - Copy.jpg', 'df d d dfd d fd', 'd d d d d d d d d ', '2017-05-27', ''),
(25, 9, 'drinks', 'green-mango-sharbat1-716x1024.jpg', '\n    Green Mango/Kacha Aam - 3 medium boiled & mashed\n    Sugar - 1/2 cup\n    Ginger - 1 teaspoon grated\n    Fresh mint leaves - 1/2 cup\n    Bit lobon/Black salt - 1 teaspoon\n    Lemon Juice from - 1 medium\n    Ice cool water - 5 cup\n', '\n    Boil, peel and mash the green mango. Let them cool.\n    In a small vessel boil sugar, ginger and 2 cups of water until sugar is dissolve. Remove from flame and let them cool.\n    In a blender pour remaining water, mango, salt, mint leaves, and sugar ', '2017-05-28', ''),
(26, 0, 'asdadsad', '18893377_1467534376639655_3837017913771112062_n.jpg', 'asdsadsadsad', 'sadsadsad', '2017-06-11', 'Pending'),
(27, 0, 'asdadsad', '18893377_1467534376639655_3837017913771112062_n.jpg', 'asdsadsadsad', 'sadsadsad', '2017-06-11', 'Approve'),
(28, 0, 'asdadsad', '18893377_1467534376639655_3837017913771112062_n.jpg', 'asdsadsadsad', 'sadsadsad', '2017-06-11', 'Cancel');

-- --------------------------------------------------------

--
-- Table structure for table `recipes`
--

CREATE TABLE `recipes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(20) NOT NULL,
  `image` varchar(255) NOT NULL,
  `ingredients` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `directions` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_featured` tinyint(1) NOT NULL,
  `is_slider` tinyint(1) NOT NULL,
  `date` date NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recipes`
--

INSERT INTO `recipes` (`id`, `name`, `category_id`, `image`, `ingredients`, `directions`, `is_featured`, `is_slider`, `date`, `is_active`) VALUES
(15, 'brekfast', 4, '14524599_1794628680753688_7253635582383656145_o.jpg', 'dff', 'sf', 0, 0, '2017-05-20', 1),
(17, 'salad', 2, '12593564_1728583094024914_557666533694921425_o.jpg', 'kkkkkkkkkkkkkk', 'nnnnnnnnnnnnnnnnnnnnnnn', 1, 1, '2017-05-20', 1),
(20, 'cold coffee', 8, 'ice-cream-563808_960_720.jpg', 'Ingredients: (1 cup = 250 ml)\r\n\r\n    1.5 tablespoons instant coffee\r\n    ¼ cup water or 62.5 ml water\r\n    4 tablespoon sugar or as required\r\n    2.5 cups milk or 625 ml milk\r\n    1 or 2 vanilla ice cream scoops for the milkshake\r\n    3 to 6 vanilla ice c', 'How to make recipe\r\nmaking coffee solution:\r\n\r\n    in a small bowl, take 1.5 tbsp instant coffee.\r\n\r\n    add ¼ cup warm or slightly hot water. \r\n\r\n    mix very well with a spoon.\r\nmaking cold coffee:\r\n\r\n    now pour this mixture in a blender jar.\r\n    add', 0, 1, '2017-05-31', 0),
(26, 'Chicken Biryani', 19, '', 'daaa', 'asass', 0, 0, '2017-06-12', 0);

-- --------------------------------------------------------

--
-- Table structure for table `recipes_category`
--

CREATE TABLE `recipes_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `is_top_nav` tinyint(1) NOT NULL,
  `date` date NOT NULL,
  `status` enum('active','inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recipes_category`
--

INSERT INTO `recipes_category` (`id`, `name`, `is_top_nav`, `date`, `status`) VALUES
(2, 'salad', 1, '2017-05-18', 'active'),
(4, 'drinks', 1, '2017-05-20', 'active'),
(8, 'vorta', 1, '2017-05-21', 'active'),
(9, 'acher', 1, '2017-05-24', ''),
(14, 'Dessert', 1, '2017-05-25', 'active'),
(15, 'Tea & coffee', 1, '2017-06-01', 'active'),
(19, 'Biriyani', 1, '2017-06-13', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `recipes_rating`
--

CREATE TABLE `recipes_rating` (
  `id` int(11) NOT NULL,
  `recipes_id` int(20) NOT NULL,
  `rating` int(20) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recipes_rating`
--

INSERT INTO `recipes_rating` (`id`, `recipes_id`, `rating`, `date`) VALUES
(1, 14, 2, '2017-05-14'),
(2, 15, 5, '2017-05-14'),
(3, 18, 3, '2017-05-14'),
(4, 3, 5, '2017-05-14'),
(5, 17, 3, '2017-05-14'),
(6, 0, 0, '2017-05-26'),
(7, 15, 4, '2017-05-26'),
(8, 15, 4, '2017-05-26'),
(9, 16, 4, '2017-05-26'),
(10, 15, 4, '2017-05-26'),
(11, 16, 4, '2017-05-26'),
(12, 15, 4, '2017-05-26'),
(13, 15, 4, '2017-05-26'),
(14, 15, 4, '2017-05-26'),
(15, 15, 4, '2017-05-26'),
(16, 15, 4, '2017-05-26'),
(17, 15, 4, '2017-05-26'),
(18, 15, 4, '2017-05-26'),
(19, 15, 4, '2017-05-26'),
(20, 15, 4, '2017-05-26'),
(21, 16, 4, '2017-05-26'),
(22, 15, 4, '2017-05-26'),
(23, 15, 4, '2017-05-26'),
(24, 15, 4, '2017-05-26'),
(25, 15, 4, '2017-05-26'),
(26, 15, 4, '2017-05-26'),
(27, 16, 4, '2017-05-26'),
(28, 15, 4, '2017-05-26'),
(29, 16, 4, '2017-05-26'),
(30, 15, 4, '2017-05-26'),
(31, 16, 4, '2017-05-26'),
(32, 16, 5, '2017-05-26'),
(33, 16, 5, '2017-05-26'),
(34, 16, 5, '2017-05-26'),
(35, 16, 5, '2017-05-26'),
(36, 16, 5, '2017-05-26'),
(37, 16, 5, '2017-05-26'),
(38, 16, 5, '2017-05-26'),
(39, 16, 5, '2017-05-26'),
(40, 15, 4, '2017-05-26'),
(41, 21, 1, '2017-05-31'),
(42, 21, 2, '2017-05-31'),
(43, 21, 3, '2017-05-31'),
(44, 21, 4, '2017-05-31'),
(45, 20, 5, '2017-05-31'),
(46, 20, 4, '2017-05-31'),
(47, 20, 1, '2017-05-31'),
(48, 20, 2, '2017-05-31'),
(49, 20, 3, '2017-05-31'),
(50, 20, 4, '2017-05-31'),
(51, 21, 5, '2017-06-07'),
(52, 21, 5, '2017-06-07'),
(53, 21, 5, '2017-06-07'),
(54, 20, 5, '2017-06-07'),
(55, 20, 5, '2017-06-07'),
(56, 21, 1, '2017-06-08'),
(57, 21, 2, '2017-06-08'),
(58, 21, 2, '2017-06-08'),
(59, 21, 3, '2017-06-08'),
(60, 21, 4, '2017-06-08'),
(61, 21, 5, '2017-06-08'),
(62, 18, 5, '2017-06-08'),
(63, 17, 5, '2017-06-08'),
(64, 15, 5, '2017-06-08'),
(65, 21, 5, '2017-06-08'),
(66, 21, 5, '2017-06-08'),
(67, 21, 5, '2017-06-08'),
(68, 21, 5, '2017-06-08'),
(69, 21, 5, '2017-06-08'),
(70, 15, 5, '2017-06-08'),
(71, 22, 1, '2017-06-12'),
(72, 22, 2, '2017-06-12'),
(73, 22, 3, '2017-06-12'),
(74, 22, 4, '2017-06-12'),
(75, 22, 5, '2017-06-12'),
(76, 26, 1, '2017-06-12'),
(77, 26, 2, '2017-06-12'),
(78, 26, 3, '2017-06-12'),
(79, 26, 4, '2017-06-12'),
(80, 26, 5, '2017-06-12'),
(81, 26, 5, '2017-06-12'),
(82, 26, 5, '2017-06-12'),
(83, 26, 5, '2017-06-12');

-- --------------------------------------------------------

--
-- Table structure for table `recipes_video`
--

CREATE TABLE `recipes_video` (
  `id` int(11) NOT NULL,
  `link` varchar(255) NOT NULL,
  `recipes_id` int(20) NOT NULL,
  `date` date NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recipes_video`
--

INSERT INTO `recipes_video` (`id`, `link`, `recipes_id`, `date`, `is_active`) VALUES
(1, 'https://www.youtube.com/embed/0Y90CXoIKh8', 17, '2017-05-22', 0),
(10, 'https://www.youtube.com/embed/TLH8fqmrHcA', 18, '2017-05-28', 1),
(11, 'https://www.youtube.com/embed/CMN9E0qaPFg', 16, '2017-05-29', 1),
(12, 'https://www.youtube.com/embed/jRzWiYRiIEw', 15, '2017-05-28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `recipes_view`
--

CREATE TABLE `recipes_view` (
  `id` int(11) NOT NULL,
  `recipes_id` int(11) NOT NULL,
  `user_ip` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recipes_view`
--

INSERT INTO `recipes_view` (`id`, `recipes_id`, `user_ip`, `date`) VALUES
(1, 15, '11111', '2017-05-14'),
(2, 1, '11111', '2017-05-14'),
(3, 18, '11111', '2017-05-14'),
(4, 20, '11111', '2017-05-14'),
(5, 17, '11111', '2017-05-14'),
(6, 14, '11111', '2017-05-14'),
(7, 22, '11111', '2017-05-14'),
(8, 21, '11111', '2017-05-14'),
(9, 20, '::1', '2017-06-01'),
(10, 20, '::1', '2017-06-01'),
(11, 20, '::1', '2017-06-01'),
(12, 0, '::1', '2017-06-01'),
(13, 0, '::1', '2017-06-02'),
(14, 0, '::1', '2017-06-03'),
(15, 0, '::1', '2017-06-04'),
(16, 0, '::1', '2017-06-05'),
(17, 0, '::1', '2017-06-05'),
(18, 21, '::1', '2017-06-05'),
(19, 21, '::1', '2017-06-06'),
(20, 21, '::1', '2017-06-07'),
(21, 21, '::1', '2017-06-08'),
(22, 21, '::1', '2017-06-11'),
(23, 20, '::1', '2017-06-11'),
(24, 20, '::1', '2017-06-11'),
(25, 21, '::1', '2017-06-11'),
(26, 21, '::1', '2017-06-11'),
(27, 21, '::1', '2017-06-11'),
(28, 21, '::1', '2017-06-11'),
(29, 21, '::1', '2017-06-11'),
(30, 21, '::1', '2017-06-11'),
(31, 21, '::1', '2017-06-11'),
(32, 21, '::1', '2017-06-11'),
(33, 21, '::1', '2017-06-11'),
(34, 20, '::1', '2017-06-11'),
(35, 21, '::1', '2017-06-11'),
(36, 21, '::1', '2017-06-12'),
(37, 22, '::1', '2017-06-12'),
(38, 22, '::1', '2017-06-12'),
(39, 22, '::1', '2017-06-12'),
(40, 22, '::1', '2017-06-12'),
(41, 22, '::1', '2017-06-12'),
(42, 22, '::1', '2017-06-12'),
(43, 22, '::1', '2017-06-12'),
(44, 22, '::1', '2017-06-12'),
(45, 22, '::1', '2017-06-12'),
(46, 22, '::1', '2017-06-12'),
(47, 22, '::1', '2017-06-12'),
(48, 26, '::1', '2017-06-13');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int(11) NOT NULL,
  `recipes_id` int(20) NOT NULL,
  `customer_id` int(20) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`id`, `recipes_id`, `customer_id`, `date`) VALUES
(1, 14, 1, '2017-05-24'),
(2, 15, 0, '2017-05-26'),
(3, 15, 0, '2017-05-26'),
(4, 16, 0, '2017-05-26'),
(5, 17, 0, '2017-05-26'),
(6, 17, 0, '2017-05-26'),
(7, 17, 0, '2017-05-26'),
(8, 15, 0, '2017-05-26'),
(9, 15, 2, '2017-05-26'),
(10, 15, 2, '2017-05-26'),
(11, 16, 2, '2017-05-26'),
(12, 16, 2, '2017-05-26'),
(13, 15, 9, '2017-05-28'),
(14, 18, 9, '2017-05-28'),
(15, 17, 9, '2017-05-28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chef_detail`
--
ALTER TABLE `chef_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chef_recipes`
--
ALTER TABLE `chef_recipes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_request`
--
ALTER TABLE `contact_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_recipes_request`
--
ALTER TABLE `customer_recipes_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recipes`
--
ALTER TABLE `recipes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recipes_category`
--
ALTER TABLE `recipes_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recipes_rating`
--
ALTER TABLE `recipes_rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recipes_video`
--
ALTER TABLE `recipes_video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recipes_view`
--
ALTER TABLE `recipes_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chef_detail`
--
ALTER TABLE `chef_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `chef_recipes`
--
ALTER TABLE `chef_recipes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `contact_request`
--
ALTER TABLE `contact_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `customer_recipes_request`
--
ALTER TABLE `customer_recipes_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `recipes`
--
ALTER TABLE `recipes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `recipes_category`
--
ALTER TABLE `recipes_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `recipes_rating`
--
ALTER TABLE `recipes_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT for table `recipes_video`
--
ALTER TABLE `recipes_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `recipes_view`
--
ALTER TABLE `recipes_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

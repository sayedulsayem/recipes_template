<!--/* Author : Munira *-->
<?php include_once './lib/settings.php'; ?>
<?php include_once './lib/connection.php'; ?>
<?php include_once './segments/header_segments.php'; ?>
<body>
    <!--preloader-->
    <div class="preloader">
        <div class="spinner"></div>
    </div>
    <!--//preloader-->

    <!--header-->
    <header class="head" role="banner">
        <!--wrap-->
        <div class="wrap clearfix">
            <a href="index.php" title="SocialChef" class="logo"><img src="images/ico/logo.png" alt="SocialChef logo" /></a>

            <!--top navbar manus item start here-->
            <?php include_once './segments/top_navbar_menu_item.php'; ?>
            <!--top navbar manus item end here-->
        </div>
    </header>
    <!--//header-->

    <!--main-->
    <main class="main" role="main">
        <!--wrap-->
        <div class="wrap clearfix">
            <!--row-->
            <div class="row">
                <!--content-->
                <section class="content center full-width">
                    <div id="msg_place"></div>
                    <div class="modal container">
                        <h3>Login</h3>
                        <div class="f-row">
                            <input type="text" name="email" placeholder="Your username" />
                        </div>
                        <div class="f-row">
                            <input type="password" name="password" placeholder="Your password" />
                        </div>

                        <div class="f-row">
                            <input type="checkbox" />
                            <label>Remember me next time</label>
                        </div>

                        <div class="f-row bwrap">
                            <input type="button" name="login" value="login" />
                        </div>
                        <p><a href="#">Forgotten password?</a></p>
                        <p>Dont have an account yet? <a href="register.php">Sign up.</a></p>
                    </div>
                </section>
                <!--//content-->
            </div>
            <!--//row-->
        </div>
        <!--//wrap-->
    </main>
    <!--//main-->


    <!--footer-->
    <?php include_once './segments/footer_part.php'; ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="ajax/login.js"></script>
</body>
</html>



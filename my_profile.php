<!--/* Author : Munira *-->
<?php
include_once './lib/settings.php';
extract($_GET);
?>
<?php include_once './lib/connection.php'; ?>
<?php include_once './segments/header_segments.php'; ?>
<body>
    <!--preloader-->
    <div class="preloader">
        <div class="spinner"></div>
    </div>
    <!--//preloader-->

    <!--header-->
    <header class="head" role="banner">
        <!--wrap-->
        <div class="wrap clearfix">
            <a href="index.html" title="SocialChef" class="logo"><img src="images/ico/logo.png" alt="SocialChef logo" /></a>

            <!--top navbar manus item start here-->
            <?php include_once './segments/top_navbar_menu_item.php'; ?>
            <!--top navbar manus item end here-->


        </div>
    </header>
    <!--//header-->

    <!--main-->
    <main class="main" role="main">
        <!--wrap-->
        <div class="wrap clearfix">
            <!--breadcrumbs-->
            <nav class="breadcrumbs">
                <ul>
                    <li><a href="index.php" title="Home">Home</a></li>
                    <li>My account</li>
                </ul>
            </nav>
            <!--//breadcrumbs-->


            <!--content-->
            <section class="content">
                <!--row-->
                <div class="content full-width">
                    <!--profile left part-->

                    <!--//profile left part-->


                    <style type="text/css">
                        @import url(http://fonts.googleapis.com/css?family=Open+Sans:700);
                        #cssmenu {
                            background: #f96e5b;
                            width: auto;
                            display: block;
                        }
                        #cssmenu ul {
                            list-style: none;
                            margin: 0;
                            padding: 0;
                            line-height: 1;
                            display: block;
                            zoom: 1;
                        }
                        #cssmenu ul:after {
                            content: " ";
                            display: block;
                            font-size: 0;
                            height: 0;
                            clear: both;
                            visibility: hidden;
                        }
                        #cssmenu ul li {
                            display: inline-block;
                            padding: 0;
                            margin: 0;
                            list-style: none;
                        }
                        #cssmenu.align-right ul li {
                            float: right;
                        }
                        #cssmenu.align-center ul {
                            text-align: center;
                        }
                        #cssmenu ul li a {
                            color: #ffffff;
                            text-decoration: none;
                            display: block;
                            padding: 15px 25px;
                            font-family: 'Open Sans', sans-serif;
                            font-weight: 700;
                            text-transform: uppercase;
                            font-size: 14px;
                            position: relative;
                            -webkit-transition: color .25s;
                            -moz-transition: color .25s;
                            -ms-transition: color .25s;
                            -o-transition: color .25s;
                            transition: color .25s;
                        }
                        #cssmenu ul li a:hover {
                            color: #333333;
                        }
                        #cssmenu ul li a:hover:before {
                            width: 100%;
                        }
                        #cssmenu ul li a:after {
                            content: "";
                            display: block;
                            position: absolute;
                            right: -3px;
                            top: 19px;
                            height: 6px;
                            width: 6px;
                            background: #ffffff;
                            opacity: .5;
                        }

                        #cssmenu ul > li::before
                        {
                            content: "";
                        }

                        #cssmenu ul li a:before {
                            content: "";
                            display: block;
                            position: absolute;
                            left: 0;
                            bottom: 0;
                            height: 3px;
                            width: 0;
                            background: #333333;
                            -webkit-transition: width .25s;
                            -moz-transition: width .25s;
                            -ms-transition: width .25s;
                            -o-transition: width .25s;
                            transition: width .25s;
                        }
                        #cssmenu ul li.last > a:after,
                        #cssmenu ul li:last-child > a:after {
                            display: none;
                        }
                        #cssmenu ul li.active a {
                            color: #333333;
                        }
                        #cssmenu ul li.active a:before {
                            width: 100%;
                        }
                        #cssmenu.align-right li.last > a:after,
                        #cssmenu.align-right li:last-child > a:after {
                            display: block;
                        }
                        #cssmenu.align-right li:first-child a:after {
                            display: none;
                        }
                        @media screen and (max-width: 768px) {
                            #cssmenu ul li {
                                float: none;
                                display: block;
                            }
                            #cssmenu ul li a {
                                width: 100%;
                                -moz-box-sizing: border-box;
                                -webkit-box-sizing: border-box;
                                box-sizing: border-box;
                                border-bottom: 1px solid #fb998c;
                            }
                            #cssmenu ul li.last > a,
                            #cssmenu ul li:last-child > a {
                                border: 0;
                            }
                            #cssmenu ul li a:after {
                                display: none;
                            }
                            #cssmenu ul li a:before {
                                display: none;
                            }
                        }




                        table {
                            background: #f5f5f5;
                            border-collapse: separate;
                            box-shadow: inset 0 1px 0 #fff;
                            font-size: 12px;
                            line-height: 24px;
                            margin: 30px auto;
                            text-align: left;
                            width: 800px;
                        }	

                        th {
                            background: url(https://jackrugile.com/images/misc/noise-diagonal.png), linear-gradient(#777, #444);
                            border-left: 1px solid #555;
                            border-right: 1px solid #777;
                            border-top: 1px solid #555;
                            border-bottom: 1px solid #333;
                            box-shadow: inset 0 1px 0 #999;
                            color: #fff;
                            font-weight: bold;
                            padding: 10px 15px;
                            position: relative;
                            text-shadow: 0 1px 0 #000;	
                        }

                        th:after {
                            background: linear-gradient(rgba(255,255,255,0), rgba(255,255,255,.08));
                            content: '';
                            display: block;
                            height: 25%;
                            left: 0;
                            margin: 1px 0 0 0;
                            position: absolute;
                            top: 25%;
                            width: 100%;
                        }

                        th:first-child {
                            border-left: 1px solid #777;	
                            box-shadow: inset 1px 1px 0 #999;
                        }

                        th:last-child {
                            box-shadow: inset -1px 1px 0 #999;
                        }

                        td {
                            border-right: 1px solid #fff;
                            border-left: 1px solid #e8e8e8;
                            border-top: 1px solid #fff;
                            border-bottom: 1px solid #e8e8e8;
                            padding: 10px 15px;
                            position: relative;
                            transition: all 300ms;
                        }

                        td:first-child {
                            box-shadow: inset 1px 0 0 #fff;
                        }	

                        td:last-child {
                            border-right: 1px solid #e8e8e8;
                            box-shadow: inset -1px 0 0 #fff;
                        }	

                        tr {
                            background: url(https://jackrugile.com/images/misc/noise-diagonal.png);	
                        }

                        tr:nth-child(odd) td {
                            background: #f1f1f1 url(https://jackrugile.com/images/misc/noise-diagonal.png);	
                        }

                        tr:last-of-type td {
                            box-shadow: inset 0 -1px 0 #fff; 
                        }

                        tr:last-of-type td:first-child {
                            box-shadow: inset 1px -1px 0 #fff;
                        }	

                        tr:last-of-type td:last-child {
                            box-shadow: inset -1px -1px 0 #fff;
                        }	

                        tbody:hover td {
                            color: transparent;
                            text-shadow: 0 0 3px #aaa;
                        }

                        tbody:hover tr:hover td {
                            color: #444;
                            text-shadow: 0 1px 0 #fff;
                        }
                    </style>


                    <?php
                    if (isset($_GET['msg'])) {
                        ?>
                        <h3 style="text-align: center; color: #o9f;"><?= $_GET['msg'] ?></h3>
                        <?php
                    }
                    elseif (isset($_GET['error'])) {
                        ?>
                        <h3 style="text-align: center; color: #f00;"><?= $_GET['error'] ?></h3>
                        <?php
                    }
                    ?>


                    <div id='cssmenu'>
                        <ul>
                            <li><a href='my_profile.php'><span>My Profile</span></a></li>
                            <li <?php if (isset($_GET['my_post'])) { ?> class='active'<?php } ?>><a href='my_profile.php?my_post'><span>My Posted Recipies</span></a></li>
                            <li <?php if (isset($_GET['approved_recipies'])) { ?> class='active'<?php } ?>><a href='my_profile.php?approved_recipies'><span>Approved Recipies</span></a></li>
                            <li <?php if (isset($_GET['wishlist'])) { ?> class='active'<?php } ?>><a href='my_profile.php?wishlist'><span>Wishlist</span></a></li>
                            <li <?php if (isset($_GET['edit_profile'])) { ?> class='active'<?php } ?>><a href='my_profile.php?edit_profile'><span>Update Profile</span></a></li>
                            <li <?php if (isset($_GET['change_password'])) { ?> class='active'<?php } ?>><a href='my_profile.php?change_password'><span>Change Password</span></a></li>
                            <li class='last'><a href='logout.php'><span>Logout</span></a></li>
                        </ul>
                    </div>

<?php
$sql = "SELECT * FROM `customer` WHERE id='$customer_id'";
foreach ($db->query($sql) as $row) :
    $profile_info = $row;
endforeach;


if (isset($_GET['my_post'])) {
    ?>
                        <h3>My Posted Recipies</h3>
                        <?php include_once './segments/my_post.php'; ?>
                        <?php
                    } elseif (isset($_GET['approved_recipies'])) {
                        ?>
                        <h3>My Approved Recipies</h3>
                        <?php include_once './segments/my_approved_recipies.php'; ?>
                        <?php
                    } elseif (isset($_GET['wishlist'])) {
                        ?>
                        <h3>My Wishlist Recipies</h3>
                        <?php include_once './segments/my_wishlist.php'; ?>
                        <?php
                    } elseif (isset($_GET['edit_profile'])) {
                        ?>

                        <form action="ajax/my_profile.php" method="post">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Full Name</th>
                                        <td><input type="text" name="name" value="<?= $profile_info['name'] ?>" placeholder="Type Full Name" /></td>
                                    </tr>
                                    <tr>
                                        <th>Email Address</th>
                                        <td><input type="text" name="email" value="<?= $profile_info['email'] ?>" placeholder="Type Email Address" /></td>
                                    </tr>

                                    <tr>
                                        <th></th>
                                        <td><input type="submit" name="modify" value="Save Changes" /></td>
                                    </tr>
                                </thead>

                            </table>
                        </form>
    <?php
} elseif (isset($_GET['change_password'])) {
    ?>

                        <form action="ajax/password.php" method="post">
                            <table>
                                <thead>

                                    <tr>
                                        <th>New Password</th>
                                        <td><input type="password" name="new_password" placeholder="Type New Password" /></td>
                                    </tr>
                                    <tr>
                                        <th>Re-Type Password</th>
                                        <td><input type="password" name="retype_password" placeholder="Type Re-Type Password" /></td>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <td><input type="submit" class="btn btn-success" name="change_password" value="Change Password" /></td>
                                    </tr>
                                </thead>

                            </table>
                        </form>
    <?php
} else {
    ?>
                        <table>
                            <thead>
                                <tr>
                                    <th>Full Name</th>
                                    <td><?= $profile_info['name'] ?></td>
                                </tr>
                                <tr>
                                    <th>Email Address</th>
                                    <td><?= $profile_info['email'] ?></td>
                                </tr>

                            </thead>

                        </table>

<?php } ?>






                    <!--//my favorites-->

                    <!--my posts-->

                    <!--//my posts-->

                </div>
                <!--//row-->
            </section>
            <!--//content-->
        </div>
        <!--//wrap-->
    </main>
    <!--//main-->


    <!--footer-->
<?php include_once './segments/footer_part.php'; ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!--//footer end-->




<?php 
$sql = "SELECT rv.*,r.name as rec_name FROM recipes_video as rv
LEFT JOIN recipes as r ON rv.recipes_id=r.id LIMIT 4";
$res = $db->query($sql);
if($res->rowCount()!=0)
{
    foreach($res as $row):
?>
<div class="entry one-fourth">
    <figure>
        
        <iframe src="<?=$row['link']?>" width="100%" frameborder="0" allowfullscreen ></iframe>
    </figure>
    <div class="container">
        <h2><a href="<?= APP_PATH ?>recipe_view.php?rec_id=<?=$row['recipes_id']?>"><?=$row['rec_name']?></a></h2> 
        <div class="actions">
            <div>
                <div class="date"><i class="fa fa-calendar"></i><a href="#"><?=date('d M Y',strtotime($row['date']))?></a></div>
                
            </div>
        </div>
    </div>
</div>
<?php 

endforeach;
} ?>


<!--item-->
<!--
item
<div class="entry one-third">
    <figure>
        <img src="images/img11.jpg" alt="" />
        <figcaption><a href="blog_single.html"><i class="icon icon-themeenergy_eye2"></i> <span>View post</span></a></figcaption>
    </figure>
    <div class="container">
        <h2><a href="blog_single.html">How to make sushi</a></h2> 
        <div class="actions">
            <div>
                <div class="date"><i class="fa fa-calendar"></i><a href="#">22 Dec 2014</a></div>
                <div class="comments"><i class="fa fa-comment"></i><a href="blog_single.html#comments">27</a></div>
            </div>
        </div>
        <div class="excerpt">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet . . . </p>
        </div>
    </div>
</div>
item

item
<div class="entry one-third">
    <figure>
        <img src="images/img10.jpg" alt="" />
        <figcaption><a href="blog_single.html"><i class="icon icon-themeenergy_eye2"></i> <span>View post</span></a></figcaption>
    </figure>
    <div class="container">
        <h2><a href="blog_single.html">Make your own bread</a></h2> 
        <div class="actions">
            <div>
                <div class="date"><i class="fa fa-calendar"></i><a href="#">22 Dec 2014</a></div>
                <div class="comments"><i class="fa fa-comment"></i><a href="blog_single.html#comments">27</a></div>
            </div>
        </div>
        <div class="excerpt">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet . . . </p>
        </div>
    </div>
</div>
item

<div class="quicklinks">
    <a href="#" class="button">More posts</a>
    <a href="javascript:void(0)" class="button scroll-to-top">Back to top</a>
</div>-->
<div class="entries row">

    <?php
    $sql = "SELECT
                                        crr.id,
                                        crr.image as recipes_image,
                                        crr.name as recipes_request_name
                                        FROM `customer_recipes_request`  as crr
                                        WHERE crr.`customer_id`='$customer_id'   AND crr.`status`='Approve'
                                        ORDER BY crr.id DESC";
    foreach ($db->query($sql) as $row) :
        ?>

        <!--item-->
        <div class="entry one-third">
            <figure>
                <img src="<?= APP_PATH ?>ajax/uploads/<?php echo $row['recipes_image']; ?>" style="height: 190px !important; width:280px;" alt="<?php echo $row['recipes_request_name']; ?>" />
            </figure>
            <div class="container">
                <h2><a href="#"><?php echo $row['recipes_request_name']; ?></a></h2> 
            </div>
        </div>
    <?php endforeach; ?>
    <!--item-->


</div>
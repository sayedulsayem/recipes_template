
<!--SELECT r.`image` as sliderImages FROM `recipes` as r WHERE r.`is_slider`=1 ORDER BY  r.id DESC  LIMIT 6-->
<div class="slider-container" id="slider">

    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <?php
        $sqlslider = "SELECT r.id,r.name,r.`image` as sliderImages FROM `recipes` as r WHERE r.`is_slider`=1 ORDER BY  r.id DESC  LIMIT 6";
        $slider = [];
        foreach ($db->query($sqlslider) as $row) {
            $slider[] = $row;
        }
        ?>

        <ol class="carousel-indicators">
            <?php
            $i = 0;
            foreach ($slider as $row):
                ?>
                <li data-target="#myCarousel" data-slide-to="<?= $i ?>" <?php echo $i ? '' : 'class="active"'; ?>></li>
                <?php
                $i++;
            endforeach;
            ?>
        </ol>


        <div class="carousel-inner">
            <?php
            $i = 0;
            foreach ($slider as $row):
                ?>
                <div class="item<?php echo $i ? '' : ' active'; ?>" >
                    <img src="<?=SITE_IMG_PATH?><?=$row['sliderImages']?>" alt="<?=$row['name']?>" style="width:100%; height:1000px;">
                    <div class="carousel-caption">
                        <h3><?=$row['name']?></h3>
                    </div>
                </div>
                <?php
                $i++;
            endforeach;
            ?>

        </div>

    </div>

</div>

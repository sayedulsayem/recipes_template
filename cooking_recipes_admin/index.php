<!--/* Author : Munira *-->
<?php 
session_start();

include_once  './lib/settings.php'; 

include_once  './lib/auth.php'; 
?>
<?php include_once  './lib/connection.php'; ?>

<?php include_once './element/headPart.php'; ?>

<title>Admin | Dashboard</title>

<body class="skin-black">
    <!-- header logo: style can be found in header.less -->
    <?php include './element/navbar.php'; ?>
    <!-- Header Navbar: style can be found in header.less -->


    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->

        <!-- Sidebar user panel -->

        <?php include_once './element/sidebar.php'; ?>

        <!-- /.sidebar -->
        <!-- Right side column. Contains the navbar and content of the page -->

        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    <?=$_SERVER['REMOTE_ADDR']?>
                    <small>Control panel</small>
                </h1>
                <!--                    <ol class="breadcrumb">
                                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                                        <li class="active">Dashboard</li>
                                    </ol>-->
            </section>
            <?php include './element/msg.php'; ?>
            <!-- Main content -->
            <?php include_once './element/dashboard.php'; ?>
        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->




    <!--footer part start here-->
    <?php include_once'./element/footer.php'; ?>    
    <!--footer part end here-->


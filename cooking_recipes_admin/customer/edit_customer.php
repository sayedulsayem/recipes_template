<!--/* Author : Munira *-->
<?php
session_start();
include_once '../lib/settings.php';
include_once  '../lib/auth.php'; 
?>
<?php include_once '../lib/connection.php'; ?>


<?php include_once '../element/headPart.php'; ?>
<title> Customers List | Dashboard</title>

<body class="skin-black">
    <!-- header logo: style can be found in header.less -->
    <?php include_once '../element/navbar.php'; ?>
    <!-- Header Navbar: style can be found in header.less -->


    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->

        <!-- Sidebar user panel -->

        <?php include_once '../element/sidebar.php'; ?>

        <!-- /.sidebar -->




        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Customers List
                    <small>Control panel</small>
                </h1>
                <!--                    <ol class="breadcrumb">
                                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                                        <li class="active">Dashboard</li>
                                    </ol>-->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Small boxes (Stat box) -->
                <div class="row">

                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center" >
                                <strong> <h4>Update  Customers List</h4></strong>
                            </div>
                            <?php
                            //build query

                            $queryup = "SELECT * FROM customer WHERE id = " . $_GET['id'];
                            //excute the query useing php
                            foreach ($db->query($queryup) as $row) {
                                $editDatapart = $row;
                            }
                            ?>
                            <?php include '../element/msg.php'; ?>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form  action="<?= APP_PATH ?>customer/update_customer_request.php" method="post" enctype="multipart/form-data">
                                            <input type="hidden" id="id" name="id" class="form-control" value="<?= $editDatapart['id']; ?>">



                                            <div class="form-group">
                                                <label for="customer_name">Customer Name</label>
                                                <input class="form-control" type="text" name="customer_name" value="<?php echo $editDatapart['name'];?>" required="required" alt="recipes_date" placeholder="Enter Customer Name  Here">

                                            </div>

                                            <div class="form-group">
                                                <label for="customer_email">Customer Email</label>
                                                <input class="form-control" type="text" name="customer_email" value="<?php echo $editDatapart['email'];?>" required="required" alt="customer_email" placeholder="EnterCustomer Email  Here ">

                                            </div>
                                            <div class="form-group">
                                                <label for="customer_join_date">Customer Join Date</label>
                                                <input class="form-control" type="text" name="customer_join_date" value="<?php echo $editDatapart['date'];?>" id="date_picker" required="required" alt="customer_join_date" placeholder="Enter Customer Join Date submit Here    YYYY-MM-DD">

                                            </div>

                                            <div class="form-group">
                                                <label>Customer status  is active/inactive now</label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="status" id="status_active"<?php if ($editDatapart['status']==1){ ?>checked="checked"<?php } ?> > Yes
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="status" id="status_inactive" <?php if ($editDatapart['status']==0){ ?>checked="checked"<?php } ?> > No
                                                </label>
                                            </div>

                                            <button type="submit" class="btn btn-default"> Update Customer Request status</button>
                                            <!--                                            <button type="reset" class="btn btn-default">Clear</button>-->
                                        </form>
                                    </div>
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>


                </div><!-- /.row (main row) -->

            </section><!-- /.content -->

        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->




    <!--footer part start here-->
    <?php include_once '../element/footer.php'; ?>    
    <!--footer part end here-->




























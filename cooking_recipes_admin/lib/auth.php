<?php 
if(!isset($_SESSION['SESS_ADMIN_ID']))
{
    session_regenerate_id();
    $_SESSION['SESS_MSG_ERROR'] = 'Session Timeout, Please Login Again.';
    session_write_close();
    header('location:' . APP_PATH . 'login.php');
    exit();
}
?>
<?php
if (isset($_SESSION['SESS_MSG_SUCCESS'])) {
    ?>
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Congrats!</b> <?= $_SESSION['SESS_MSG_SUCCESS'] ?>
    </div>
    <?php
    
    unset($_SESSION['SESS_MSG_SUCCESS']);
}


if (isset($_SESSION['SESS_MSG_ERROR'])) {
    ?>
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Error!</b> <?= $_SESSION['SESS_MSG_ERROR'] ?>
    </div>
    <?php
    unset($_SESSION['SESS_MSG_ERROR']);
}
<!--/* Author : Munira *-->

<?php
session_start();
include_once '../lib/settings.php';

include_once '../lib/connection.php';
include_once '../lib/auth.php';
?>


<?php include_once '../element/headPart.php'; ?>
<title>View List Recipes | Dashboard</title>

<body class="skin-black">
    <!-- header logo: style can be found in header.less -->
    <?php include_once '../element/navbar.php'; ?>
    <!-- Header Navbar: style can be found in header.less -->


    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->

        <!-- Sidebar user panel -->

        <?php include_once '../element/sidebar.php'; ?>

        <!-- /.sidebar -->




        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Recipes List show
                    <small>Control panel</small>
                </h1>
                <!--                    <ol class="breadcrumb">
                                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                                        <li class="active">Dashboard</li>
                                    </ol>-->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Small boxes (Stat box) -->
                <div class="row">

                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center">
                                <strong> <h4> Recipes List show</h4></strong>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table width="100%" class="table table-striped table-bordered table-hover table-responsive">

                                            <tbody>
                                                <?php
                                                $sql = "SELECT * FROM recipes WHERE id = " . $_GET['id'];
                                                foreach ($db->query($sql) as $sqlresult):
                                                    ?>


                                                    <tr>
                                                   
                                                    <tr><td>Recipes Image</td><td><img src="<?= APP_PATH ?>images/<?php echo $sqlresult['image']; ?>" style="width:500px;height:300px; border:2px inset black;" class="img-thumbnail img-responsive"></td></tr>
                                                    <tr><td>Recipes ID</td><td><?php echo $sqlresult['id']; ?></td></tr>
                                                    <tr><td>Recipes Name</td><td><?php echo $sqlresult['name']; ?></td></tr>
                                                    <tr><td>Recipes Ingredients</td><td><?php echo $sqlresult['ingredients']; ?></td></tr>
                                                    <tr><td>Recipes Directions</td><td><?php echo $sqlresult['directions']; ?></td></tr>
                                                    <tr><td>Recipes is slider</td><td><?php echo ($sqlresult['is_slider']) ? 'active' : 'inactive'; ?></td></tr>
                                                    <tr><td>Recipes status</td><td> <?php echo ($sqlresult['is_active']) ? 'active' : 'inactive'; ?></td></tr>
                                                    <tr><td>Recipes Date</td><td><?php echo $sqlresult['date']; ?></td></tr>
                                 
                                                    </tr>
                                                    <?php
                                                endforeach;
                                                //}
                                                ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>


                </div><!-- /.row (main row) -->

            </section><!-- /.content -->

        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->




    <!--footer part start here-->
    <?php include_once '../element/footer.php'; ?>    
    <!--footer part end here-->




























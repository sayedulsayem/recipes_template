/* Author : Munira */

$(document).ready(function () {


    //rating js start from here
    $('.full').click(function () {
        //alert('dd');
        var rating = 0;
        if ($(this).attr("for") == "star5")
        {
            rating = 5;
        }
        if ($(this).attr("for") == "star4")
        {
            rating = 4;
        }
        if ($(this).attr("for") == "star3")
        {
            rating = 3;
        }
        if ($(this).attr("for") == "star2")
        {
            rating = 2;
        }
        if ($(this).attr("for") == "star1")
        {
            rating = 1;
        }

        var rec_id = $(this).attr("data");


        $.post("./ajax/rating.php", {'recipes_id': rec_id, 'rating': rating}, function (data) {
            $("#rating_result_" + rec_id).html(data);
            //+ rec_id means we are concating data with string
        });


    });
    //rating js ends here
});